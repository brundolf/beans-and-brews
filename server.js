// express
var express = require('express');

// application
var app = express();
app.use(express.static('public'));

// templating engine
var swig = require('swig');
swig.setDefaults({ cache: false });

app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('view cache', false);

// routes
app.get('/', function(request, response) {
  response.render('index.html');
});
app.get('/contact', function(request, response) {
  response.render('contact.html');
});
app.get('/beans', function(request, response) {
  response.render('beans.html');
});
app.get('/brews', function(request, response) {
  response.render('brews.html');
});

// start
app.set('port', (process.env.PORT || 8001));
app.listen(app.get('port'));
