
(function(){

  var fadeLinks = document.getElementsByClassName("fade-link");
  for(var i = 0; i < fadeLinks.length; i++){
      var elem = fadeLinks[i];
      elem.onclick = function(){// Don't go to the next page yet.
          event.preventDefault();
          linkLocation = this.href;

          document.getElementsByTagName('body')[0].classList.add('fadeout');
          setTimeout(function(){
            window.location = linkLocation;
          }, 500);

          return false;
      };
  }

})();
